package innovationsquare.com.alternatebrain.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.uncopt.android.widget.text.justify.JustifiedTextView;

import innovationsquare.com.alternatebrain.R;

/**
 * Created by tariq on 11/10/2017.
 */

public class FragmentInfo extends Fragment {

    EditText name, username, email, sector, city, country, phone;
    TextView changePassword;
    String usernameDummy;
    public View view;
    public RelativeLayout updateProfile;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_info, container, false);
        JustifiedTextView myMsg = (JustifiedTextView)view.findViewById(R.id.t1);
        myMsg.setText(R.string.infoStr);
//        JustifiedTextView jtv= new JustifiedTextView(getActivity(), "\n" +
//                "This APP will be the second brain for \n1) Users: To do most of the decision making and calculations automatically like how to plan next month’s budget, where to find the cheapest product, how to save income tax etc. Now all above will be handled by this App. You just sit and relax. 2) Corporate World: To see buying behavior/ pattern, selling trend, customer segmentation and categorization, footfall and rating, prediction and forecasting, target campaigning, Market Basket analysis, text analytics, link analysis and last but not the least Social Media Analytics. For more details, please email our support team at support_ab@alternatebrain.com");
//        LinearLayout place = (LinearLayout) view.findViewById(R.id.book_profile_content);
//        place.addView(jtv);

        return view;
    }

}