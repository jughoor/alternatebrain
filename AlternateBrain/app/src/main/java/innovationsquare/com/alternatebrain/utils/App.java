package innovationsquare.com.alternatebrain.utils;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

import innovationsquare.com.alternatebrain.services.NetworkChangeReceiver;

/**
 * Created by tariq on 9/9/2017.
 */

public class App extends Application {

    private static App instance;
    public BroadcastReceiver broadcastReceiver;
    public static App getInstance() {
        return instance;
    }
    //private EventBus eventBus = EventBus.getDefault();

    @Override
    public void onCreate() {
        super.onCreate();
        //broadcastReceiver = new NetworkChangeReceiver();
        //IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        //registerReceiver(broadcastReceiver, intentFilter);
        //eventBus.register(this);
//        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
//        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                Toast.makeText(context, "dfsdfsd", Toast.LENGTH_SHORT).show();
//            }
//        };
//        registerReceiver(broadcastReceiver, intentFilter);
        instance = this;
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/avenir-roman.ttf");
    }
}