
package innovationsquare.com.alternatebrain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Image {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("attachment")
    @Expose
    public String attachment;
    @SerializedName("user_id")
    @Expose
    public Integer userId;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;

}
