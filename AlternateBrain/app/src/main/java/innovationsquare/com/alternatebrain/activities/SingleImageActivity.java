package innovationsquare.com.alternatebrain.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.bumptech.glide.Glide;

import innovationsquare.com.alternatebrain.R;
import innovationsquare.com.alternatebrain.utils.TouchImageView;
import innovationsquare.com.alternatebrain.utils.URL;

public class SingleImageActivity extends AppCompatActivity {

    private TouchImageView imgShown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_image);
        String data = getIntent().getExtras().getString("imageKey","");
        imgShown = (TouchImageView) findViewById(R.id.imgShown);
        if(data.length() > 0){
            //Picasso.with(this).load(URL.IMAGE_BASE+data).into(imgShown);
            Glide.with(this)
                    .load(URL.IMAGE_BASE+data)
                    .into(imgShown);
        }
        imgShown.setOnTouchImageViewListener(new TouchImageView.OnTouchImageViewListener() {

            @Override
            public void onMove() {
            }
        });
    }
}
